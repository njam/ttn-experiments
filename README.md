ttn-experiments
===============

A very simple [TTN](https://www.thethingsnetwork.org/) application for testing purposes, written in NodeJS.

Local Development
-----------------

Install dependencies:
```
yarn install
```

Run:
```
node --experimental-modules index.mjs
```

TTN CLI
-------

Connect to the app with [ttnctl](https://www.thethingsnetwork.org/docs/network/cli/quick-start.html):
```
ttnctl applications select ttn-experiments
```

Create a test device:
```
ttnctl devices register test-dev-1
```

Simulate an uplink message "01FF" (511) on port "1":
```
ttnctl devices simulate test-dev-1 --port 1 '01FF'
```

Heroku Deployment
-----------------

App is deployed to Heroku via Gitlab CI:
https://dashboard.heroku.com/apps/ttn-experiments

### Setup Local Environment

Login to Heroku:
```
heroku login
```

Add a Git remote to link the folder to the  Heroku app:
```
heroku git:remote -a ttn-experiments
```
