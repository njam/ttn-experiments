import ttn from 'ttn'
import fs from 'fs'

class Application {

  /**
   * @param {String} appId
   * @param {String} accessKey
   * @param logger
   */
  constructor (appId, accessKey, logger) {
    this._logger = logger
    this._appId = appId
    this._accessKey = accessKey
  }

  async run () {
    let app = await ttn.application(this._appId, this._accessKey)
    await app.setCustomPayloadFunctions({
      decoder: fs.readFileSync('src/payload-functions/UplinkDecoder.js', 'utf8'),
      converter: fs.readFileSync('src/payload-functions/UplinkConverter.js', 'utf8'),
      validator: fs.readFileSync('src/payload-functions/UplinkValidator.js', 'utf8'),
      encoder: fs.readFileSync('src/payload-functions/DownlinkEncoder.js', 'utf8'),
    })

    this._client = await ttn.data(this._appId, this._accessKey)
    this._client.on('uplink', (deviceId, data) => {
      this._handleMessage(deviceId, data['port'], data['payload_fields'])
    })
  }

  /**
   * @param {String} deviceId
   * @param {Number} port
   * @param {Object} payload
   * @private
   */
  _handleMessage (deviceId, port, payload) {
    this._logger.info('Received: ', {
      port: port,
      payload: payload
    })
    let foo = payload['foo']
    this._sendMessage(deviceId, {foo: foo * 2})
  }

  /**
   *
   * @param {String} deviceId
   * @param {Object} payload
   * @private
   */
  _sendMessage (deviceId, payload) {
    this._client.send(deviceId, payload)
  }

}

export default Application
