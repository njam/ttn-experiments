function Encoder (object, port) {
  var bytes = []
  if (port === 1) {
    bytes = bytes.concat(intToBytes(object['foo'], 2))
  }
  return bytes
}

function intToBytes (integer, numBytes) {
  var bytes = []
  for (var n = numBytes - 1; n >= 0; n--) {
    bytes[n] = integer & (255)
    integer = integer >> 8
  }
  return bytes
}
