function Decoder (bytes, port) {
  var decoded = {}
  if (port === 1) {
    if (bytes.length === 2) {
      decoded['foo'] = bytesToInt(bytes.slice(0, 2))
    }
  }
  return decoded
}

function bytesToInt (bytes) {
  var integer = 0
  for (var n = 0; n < bytes.length; n++) {
    integer += bytes[n]
    if (n < bytes.length - 1) {
      integer = integer << 8
    }
  }
  return integer
}
