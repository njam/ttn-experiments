import caporal from 'caporal'
import Application from '../src/Application'

caporal
  .argument('<appId>', 'TTN Application ID')
  .argument('<accessKey>', 'TTN Application Access Key')
  .action(async function ({appId, accessKey}, options, logger) {
    let application = new Application(appId, accessKey, logger)
    logger.info(`Running application "${appId}".`)
    await application.run()
  })

caporal.parse(process.argv)
