FROM node:10

WORKDIR /opt/ttn-experiments

# Install dependencies
COPY package.json yarn.lock ./
RUN yarn install

# Copy code
COPY bin ./bin
COPY src ./src

ENTRYPOINT []
CMD node --experimental-modules bin/index.mjs "${TTN_APP_ID}" "${TTN_ACCESS_KEY}"
